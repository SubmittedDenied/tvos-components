module.exports = {
  AlertTemplate: require('./src/templates/alert-template'),
  MainTemplate: require('./src/templates/main-template'),
  MenuBarTemplate: require('./src/templates/menu-bar-template'),
  ListTemplate: require('./src/templates/list-template'),
  CatalogTemplate: require('./src/templates/catalog-template'),
  ProductBundleTemplate: require('./src/templates/product-bundle-template'),
  ButtonComponent: require('./src/components/button-component')
};
