var _ = require('underscore');
var BaseComponent = require('../components/base-component');
var TextComponent = require('../components/text-component');

var ButtonComponent = module.exports = function(options) {
  this.element = new BaseComponent('button', options);
  this.element.addChild(
    (new BaseComponent('text')).addChild(
      new TextComponent(options.text)
    )
  );
}

ButtonComponent.prototype.render = function(doc) {
  return this.element.render(doc);
}
