var _ = require('underscore');
var BaseComponent = require('./base-component');

// options: {
//   children: [
//    menuItemComponent({document: <doc>})
//   ]
// }
var MenuBarComponent = module.exports = function(options) {
  BaseComponent.call(this, 'menuBar', options);
}
MenuBarComponent.prototype = Object.create(BaseComponent.prototype);

MenuBarComponent.prototype.render = function(doc) {
  var result = BaseComponent.prototype.render.call(this, doc);
  var self = this;
  this._mbd = this._ele.getFeature('MenuBarDocument');
  _.each(this.children, function(child) {
    self._mbd.setDocument(child.getDocument(), child._ele);
  });

  return result;
}
