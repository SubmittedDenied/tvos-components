var _ = require('underscore');
var BaseComponent = require('../components/base-component');
var TextComponent = require('../components/text-component');

var SubtitleComponent = module.exports = function(options) {
  BaseComponent.call(this, 'subtitle', options);
  this.addChild(
    new TextComponent(this.options.title)
  );
}
SubtitleComponent.prototype = Object.create(BaseComponent.prototype);
