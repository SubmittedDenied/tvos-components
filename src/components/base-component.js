var _ = require('underscore');

var BaseComponent = module.exports = function(nodeName, options) {
  this.nodeName = nodeName;
  this.children = [];
  this.options = _.extend({}, options);
}

BaseComponent.prototype.addChild = function(element) {
  this.children.push(element);
  return this;
}

BaseComponent.prototype.render = function(doc) {
  var ele = doc.createElement(this.nodeName);
  if(this.options.events) {
    _.each(this.options.events, function(listener, event) {
      ele.addEventListener(event, listener);
    });
  }
  _.each(this.options.attributes, function(value, name) {
    ele.setAttribute(name, value);
  });
  _.each(this.children, function(child) {
    ele.appendChild(
      child.render(doc)
    );
  });

  this._ele = ele;
  return ele;
}

BaseComponent.prototype.setAttribute = function(key, value) {
  if(this._ele) {
    this._ele.setAttribute(key, value);
  }

  this.options.attributes = this.options.attributes || {};
  this.options.attributes[key] = value;
}
