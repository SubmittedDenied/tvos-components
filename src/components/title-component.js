var _ = require('underscore');
var BaseComponent = require('../components/base-component');
var TextComponent = require('../components/text-component');

var TitleComponent = module.exports = function(options) {
  BaseComponent.call(this, 'title', options);
  this.addChild(
    new TextComponent(this.options.title)
  );
}
TitleComponent.prototype = Object.create(BaseComponent.prototype);
