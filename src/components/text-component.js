var TextComponent = module.exports = function(text) {
  this.text = text;
}

TextComponent.prototype.addChild = function(element) {
  //noop
  return this;
}

TextComponent.prototype.render = function(doc) {
  this._node = doc.createTextNode(this.text);
  return this._node;
}

TextComponent.prototype.updateText = function(newText) {
  if(this._node) {
    this._node.innerHTML = newText;
  }
  this.text = newText;
}
