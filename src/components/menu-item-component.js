var _ = require('underscore');
var BaseComponent = require('./base-component');
var TextComponent = require('./text-component');

// options: {
//  title: "foo",
//  document: <doc>
// }
var MenuItemComponent = module.exports = function(options) {
  BaseComponent.call(this, 'menuItem', options);
  this.addChild((new BaseComponent('title')).addChild(new TextComponent(options.title)));
}
MenuItemComponent.prototype = Object.create(BaseComponent.prototype);

MenuItemComponent.prototype.getDocument = function() {
  return this.options.document;
}

