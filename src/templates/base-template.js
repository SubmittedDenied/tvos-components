var BaseComponent = require('../components/base-component');

var BaseTemplate = module.exports = function(rootNodeName, options) {
  this.rootEle = new BaseComponent(rootNodeName, options);
}

BaseTemplate.prototype.addChild = function(child) {
  this.rootEle.addChild(child);
  return this;
}

BaseTemplate.prototype.render = function() {
  var xmlString = `<?xml version="1.0" encoding="UTF-8" ?>
    <document>
    </document>
  `;

  var parser = new DOMParser();
  var doc = parser.parseFromString(xmlString, "application/xml");
  var root = this.rootEle.render(doc);

  doc.getElementsByTagName('document').item(0).appendChild(root);

  return doc;
}
