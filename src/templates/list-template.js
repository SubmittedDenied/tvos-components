var _ = require('underscore');
var BaseTemplate = require('./base-template');
var BaseComponent = require('../components/base-component');
var TitleComponent = require('../components/title-component');
var SubtitleComponent = require('../components/subtitle-component');
var TextComponent = require('../components/text-component');

// options: {
//   backgroundImg: 'url',
//   title: 'Some text',
//   items: [
//     {
//       text: 'foo',
//       title: 'Some Foo',
//       subtitle: 'Here is a thing about Foo',
//       img: 'http://img/src.png', //optional
//       ordinal: '1' //optional
//     }
//   ]
// }
var ListTemplate = module.exports = function(options) {
  this.options = options;
}

ListTemplate.prototype.render = function() {
  var result = new BaseTemplate('listTemplate', this.options);
  var self = this;

  result.addChild(new BaseComponent('banner'));
  var listComp = new BaseComponent('list');
  var heroImg = new BaseComponent('heroImg', {attributes: {src: this.options.items[0].img || this.options.backgroundImg}});
  listComp.addChild(
    (new BaseComponent('header')).addChild(
      new TitleComponent({title: this.options.title})
    )
  ).addChild(
    (new BaseComponent('relatedContent')).addChild(
      (new BaseComponent('itemBanner')).addChild(
        (heroImg)
      )
    )
  );
  var section = new BaseComponent('section');
  _.each(this.options.items, function(item) {
    var component = new BaseComponent('listItemLockup', item);
    if(item.ordinal) {
      component.addChild(
        (new BaseComponent('ordinal')).addChild(new TextComponent(item.ordinal))
      );
    }
    component.addChild(new TitleComponent({title: item.text}));

    var lockup = new BaseComponent('lockup');
    component.addChild(
      (new BaseComponent('relatedContent')).addChild(lockup)
    );
    if(item.img) {
      lockup.addChild(
        new BaseComponent('img', {attributes: {src: item.img}})
      );
    }
    if(item.title) {
      lockup.addChild(
        new TitleComponent({title: item.title})
      ) 
    }

    if(item.subtitle) {
      var params = item.fontSize ? {
        attributes: {
          style:"font-size:" + item.fontSize + "px;"
        }
      }:{};

      lockup.addChild(
        (new BaseComponent('description', params)).addChild(
          new TextComponent(item.subtitle)
        )
      );
    }

    section.addChild(component);
  });

  listComp.addChild(section);

  result.addChild(listComp);

  return result.render();
}
