var _ = require('underscore');
var BaseTemplate = require('./base-template');
var MenuBarComponent = require('../components/menu-bar-component');
var MenuItemComponent = require('../components/menu-item-component');

// options: {
//   items: [
//     {title: 'foo', document: <document>}
//   ]
// }
var MenuBarTemplate = module.exports = function(options) {
  this.options = options;
}

MenuBarTemplate.prototype.render = function() {
  var result = new BaseTemplate('menuBarTemplate', this.options);
  var self = this;

  var bar = new MenuBarComponent();
  _.each(this.options.items, function(child) {
    var item = new MenuItemComponent(child)
    bar.addChild(item);
  });

  result.addChild(bar);

  return result.render();
}
