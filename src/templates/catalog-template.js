var _ = require('underscore');
var BaseTemplate = require('./base-template');
var BaseComponent = require('../components/base-component');
var TitleComponent = require('../components/title-component');
var SubtitleComponent = require('../components/subtitle-component');
var TextComponent = require('../components/text-component');

// options: {
//   title: 'What to eat',
//   tags: [],
//   items: [
//     {
//       title: 'Pizza by Alfredo',
//       img: 'http://img/src.png',
//       distance: '10 miles',
//       info: '2800 E Observatory Rd\nLos Angeles, CA 90027\n(213) 473-0800',
//       tags: ['Host recommendations' ....]
//     }
//   ]
// }
var CatalogTemplate = module.exports = function(options) {
  this.options = options;
}

CatalogTemplate.prototype.render = function() {
  var result = new BaseTemplate('catalogTemplate', this.options);
  var self = this;

  result.addChild(
    (new BaseComponent('banner')).addChild(
      new TitleComponent({title: this.options.title})
    )
  );
  var listComp = new BaseComponent('list');
  var section = new BaseComponent('section');
  listComp.addChild(section);

  result.addChild(listComp);

  _.each(this.options.tags, function(tag) {
    var itemsInTag = _.filter(self.options.items, function(item) {
      return _.contains(item.tags, tag);
    });

    var component = new BaseComponent('listItemLockup');
    component.addChild(new TitleComponent({title: tag}));
    component.addChild((new BaseComponent('decorationLabel')).addChild(new TextComponent(itemsInTag.length)));

    var innerSection = new BaseComponent('section');
    component.addChild(
      (new BaseComponent('relatedContent')).addChild(
        (new BaseComponent('grid')).addChild(
          innerSection
        )
      )
    );
    _.each(itemsInTag, function(item) {
      var itemComponent = (new BaseComponent('lockup', item)).addChild(
          new BaseComponent('img', {attributes: {src: item.img, width: 500, height: 500}})
        ).addChild(
          new TitleComponent({title: item.title})
        );

      innerSection.addChild(itemComponent);
    });
    section.addChild(component);
  });


  return result.render();
}
