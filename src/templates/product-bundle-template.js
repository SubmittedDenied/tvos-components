var _ = require('underscore');
var BaseTemplate = require('./base-template');
var BaseComponent = require('../components/base-component');
var TitleComponent = require('../components/title-component');
var SubtitleComponent = require('../components/subtitle-component');
var TextComponent = require('../components/text-component');

// options: {
//  title: 'Pizza by Alfredo',
//  img: 'http://img/src.png',
//  distance: '10 miles away',
//  info: [
//    '2800 E Observatory Rd',
//    'Los Angeles, CA 90027',
//    '(213) 473-0800'
//  ],
//  images: []
// }
var ProductBundleTemplate = module.exports = function(options) {
  this.options = options;
}

ProductBundleTemplate.prototype.render = function() {
  var result = new BaseTemplate('productBundleTemplate', this.options);
  var self = this;

  result.addChild(
    (new BaseComponent('background')).addChild(
      new BaseComponent('img', {attributes: {src: this.options.img}})
    )
  );
  var banner = new BaseComponent('banner');
  var stack = (new BaseComponent('stack')).addChild(
      new TitleComponent({title: this.options.title})
    ).addChild(
      (new BaseComponent('row')).addChild(
        (new BaseComponent('text')).addChild(new TextComponent(this.options.distance))
      )
    );
  _.each(this.options.info, function(line) {
    stack.addChild(
      (new BaseComponent('text')).addChild(new TextComponent(line))
    );
  });
  banner.addChild(stack);

  result.addChild(banner);

  if(this.options.images && this.options.images.length > 0) {
    var shelf = new BaseComponent('shelf');
    var section = new BaseComponent('section');
    _.each(this.options.images, function(image) {
      section.addChild(
        (new BaseComponent('lockup')).addChild(
          new BaseComponent('img', {attributes: {src: image}})
        )
      );
    });

    shelf.addChild(section);
    result.addChild(shelf);
  }

  return result.render();
}
