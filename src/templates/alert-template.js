var _ = require('underscore');
var BaseTemplate = require('./base-template');
var BaseComponent = require('../components/base-component');
var TextComponent = require('../components/text-component');
var ButtonComponent = require('../components/button-component');

var AlertTemplate = module.exports = function(options) {
  this.options = options;
}

AlertTemplate.prototype.render = function() {
  var result = new BaseTemplate('alertTemplate', this.options);
  var self = this;

  _.each(['title', 'description', 'text'], function(key) {
    if(self.options[key]) {
      result.addChild(
        (new BaseComponent(key)).addChild(new TextComponent(self.options[key]))
      );
    }
  });

  if(this.options.buttons) {
    var buttons = _.each(this.options.buttons, function(btnSpec) {
      var button = new ButtonComponent(btnSpec);
      result.addChild(button);
    });
  }

  return result.render();
}
