var _ = require('underscore');
var BaseTemplate = require('./base-template');

var DivTemplate = module.exports = function(options) {
  this.options = options;
  this._children = [];
}

DivTemplate.prototype.addChild = function(component) {
  this._children.push(component);
}

DivTemplate.prototype.render = function() {
  var result = new BaseTemplate('divTemplate', this.options);
  var self = this;

  _.each(this._children, function(child) {
    result.addChild(child);
  });

  return result.render();
}
