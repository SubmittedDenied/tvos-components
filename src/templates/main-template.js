var _ = require('underscore');
var BaseTemplate = require('./base-template');
var BaseComponent = require('../components/base-component');
var TextComponent = require('../components/text-component');

// options: {
//   backgroundImg: "path to background image",
//   menuItems: [
//     {title: "foo", events: { highlight: function() {...} } },
//     ...
//   ]
// }
var MainTemplate = module.exports = function(options) {
  this.options = options;
  this._template = new BaseTemplate('mainTemplate', this.options);
  var self = this;

  if(this.options.backgroundImg) {
    this._imageComponent = new BaseComponent('img', {
      attributes: {
        src: this.options.backgroundImg
      }
    })
    this._template.addChild(
      (new BaseComponent('background')).addChild(this._imageComponent)
    )
  }

  if(this.options.menuItems) {
    var section = new BaseComponent('section');
    _.each(this.options.menuItems, function(item) {
      section.addChild(
        (new BaseComponent('menuItem', {events: item.events})).addChild(
          (new BaseComponent('title')).addChild(
            new TextComponent(item.title)
          )
        )
      )
    });

    this._template.addChild(
      (new BaseComponent('menuBar')).addChild(section)
    );
  }
}

MainTemplate.prototype.render = function() {
  return this._template.render();
}

MainTemplate.prototype.replaceImage = function(imgSrc) {
  this._imageComponent.setAttribute('src', imgSrc);
}

MainTemplate.prototype.resetImage = function() {
  this._imageComponent.setAttribute('src', this.options.backgroundImg);
}
