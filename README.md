#tvos-components
##Installation

`npm install --save tvos-components`

##Components
###ListTemplate
Renders a ListTemplate document

```
{
  backgroundImg: 'url',
  title: 'Some text',
  items: [
    {
      text: 'foo',
      title: 'Some Foo',
      subtitle: 'Here is a thing about Foo',
      img: 'http://img/src.png'
    }
  ]
}
```
